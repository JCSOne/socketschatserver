﻿using ChatServer.Models;

namespace ChatServer
{
    class Program
    {
        static void Main(string[] args)
        {
            var chatModel = new ChatModel();
            chatModel.Start();
        }
    }
}
