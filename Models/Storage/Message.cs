using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChatServer.Models.Storage
{
    public class Message
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime SentDate { get; set; }
        public int SenderId { get; set; }
        [ForeignKey(nameof(SenderId))]
        public User Sender { get; set; }
        public int RecipientId { get; set; }
        [ForeignKey(nameof(RecipientId))]
        public User Recipient { get; set; }
    }
}