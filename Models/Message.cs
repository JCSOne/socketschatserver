using System;
using Newtonsoft.Json;

namespace ChatServer.Models
{
    public class Message
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("content")]
        public string Content { get; set; }
        [JsonProperty("sent-datetime")]
        public DateTime SentDateTime { get; set; }
        [JsonProperty("sender-id")]
        public int SenderId { get; set; }
        [JsonProperty("recipient-id")]
        public int? RecipientId { get; set; }
    }
}